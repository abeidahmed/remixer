'use strict';

module.exports = {
  root: true,
  plugins: ['prettier'],
  extends: ['eslint:recommended', 'prettier'],
  reportUnusedDisableDirectives: true,
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  env: {
    browser: true,
  },
  overrides: [
    {
      files: ['.eslintrc.cjs', '.prettierrc.cjs', 'webpack.config.cjs'],
      env: {
        node: true,
      },
    },
  ],
  rules: {
    'prettier/prettier': ['error'],
  },
};
