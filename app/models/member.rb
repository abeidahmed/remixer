class Member < ApplicationRecord
  belongs_to :organization
  belongs_to :user

  validates :user, uniqueness: {scope: :organization_id, message: "is already in the organization"}
end
