class Organization < ApplicationRecord
  belongs_to :owner, class_name: "User", foreign_key: :owner_id
  has_many :members, dependent: :destroy

  validates :name, presence: true, length: {maximum: 255}
end
