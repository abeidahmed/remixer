class RegistrationsController < Devise::RegistrationsController
  def create
    super
    flash.delete(:notice)
  end

  def after_sign_up_path_for(resource)
    stored_location_for(resource) || new_organization_path
  end
end
