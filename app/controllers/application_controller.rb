class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!

  private

  def set_current_organization(organization)
    cookies.signed[:organization_id] = {value: organization.id, expires: 1.year}
  end

  def current_organization
    @current_organization ||= Organization.find_by(id: cookies.signed[:organization_id])
  end
  helper_method :current_organization

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[name])
  end

  def render_json_errors(errors, status: :unprocessable_entity)
    render json: ErrorSerializer.serialize(errors), status: status
  end
end
