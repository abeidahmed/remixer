class App::ApplicationController < ApplicationController
  before_action :verify_organization

  private

  def verify_organization
    unless current_organization
      redirect_to root_path, status: :unauthorized
    end
  end
end
