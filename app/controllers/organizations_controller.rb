class OrganizationsController < ApplicationController
  def index
    render :index, layout: "slate"
  end

  def new
    @organization = current_user.owned_organizations.build
    render :new, layout: "slate"
  end

  def create
    organization = current_user.owned_organizations.build(organization_params)
    organization.members.build(user: current_user)

    if organization.save
      set_current_organization(organization)
      redirect_to dashboards_path
    else
      render_json_errors(organization.errors)
    end
  end

  private

  def organization_params
    params.require(:organization).permit(:name)
  end
end
