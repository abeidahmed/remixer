class AvatarComponent < ApplicationComponent
  def initialize(src:, alt:, size: 32, square: false, **options)
    @options = options
    @options[:tag] = :img
    @options[:src] = src
    @options[:alt] = alt
    @options[:height] = size
    @options[:width] = size
    @options[:class] = class_names(
      options[:class],
      "avatar",
      "avatar--rounded": !square
    )
  end

  def call
    render(BaseComponent.new(**@options))
  end
end
