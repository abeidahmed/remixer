module Forms
  class CheckBoxComponent < ApplicationComponent
    def initialize(object_name, method_name, checked_value: "1", unchecked_value: "0", **options)
      @object_name = object_name
      @method_name = method_name
      @checked_value = checked_value
      @unchecked_value = unchecked_value
      @options = options
      # Avoid persisting checked state on Firefox
      @options[:autocomplete] ||= "off"
      @options[:class] = class_names(options[:class], "form-check-input")
    end

    def call
      @view_context.check_box(@object_name, @method_name, @options, @checked_value, @unchecked_value)
    end
  end
end
