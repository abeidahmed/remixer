module Forms
  class TextFieldComponent < ApplicationComponent
    def initialize(object_name, method_name, help_text: nil, **options)
      @object_name = object_name
      @method_name = method_name
      @help_text = help_text
      @help_text_id = SecureRandom.urlsafe_base64
      @options = options
      @options["aria-describedby"] = @help_text_id if @help_text
      @options[:type] ||= "text"
      @options[:class] = class_names(options[:class], "form-control")
    end
  end
end
