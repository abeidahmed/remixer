module Forms
  class HelpTextComponent < ApplicationComponent
    def initialize(**options)
      @options = options
      @options[:tag] ||= :span
      @options[:class] = class_names(options[:class], "form-text")
    end

    def call
      render(BaseComponent.new(**@options)) { content }
    end

    def render?
      content.present?
    end
  end
end
