module Forms
  class LabelComponent < ApplicationComponent
    def initialize(object_name, method_name, text = nil, checkable: false, **options)
      @object_name = object_name
      @method_name = method_name
      @text = text
      @options = options
      @options[:class] = class_names(
        options[:class],
        "form-check-label": checkable,
        "form-label": !checkable
      )
    end

    def call
      if content.present?
        @view_context.label(@object_name, @method_name, @text, **@options) { content }
      else
        @view_context.label(@object_name, @method_name, @text, **@options)
      end
    end
  end
end
