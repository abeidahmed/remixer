class ButtonComponent < ApplicationComponent
  DEFAULT_VARIANT = :default
  VARIANT_MAPPINGS = {
    DEFAULT_VARIANT => "btn-default",
    :primary => "btn-primary"
  }.freeze

  def initialize(tag: :button, variant: DEFAULT_VARIANT, full: false, disabled: false, **options)
    @options = options
    @options[:tag] = tag
    @options[:type] ||= :button if tag == :button
    @options[:disabled] = "true" if disabled
    @options["aria-disabled"] = "true" if disabled
    @options[:tabindex] = "-1" if disabled
    @options[:href] = nil if disabled

    @options[:class] = class_names(
      options[:class],
      "btn",
      VARIANT_MAPPINGS[fetch_or_fallback(VARIANT_MAPPINGS.keys, variant, DEFAULT_VARIANT)],
      "w-100": full,
      disabled: disabled
    )
  end

  def call
    render(BaseComponent.new(**@options)) { content }
  end
end
