class BaseComponent < ViewComponent::Base
  def initialize(tag:, **options)
    @tag = tag
    @options = options
  end

  def call
    content_tag(@tag, content, **@options)
  end
end
