class ApplicationComponent < ViewComponent::Base
  private

  def fetch_or_fallback(variant_keys, given_value, default_value)
    if given_value && variant_keys.include?(given_value)
      given_value
    else
      default_value
    end
  end
end
