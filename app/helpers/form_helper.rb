module FormHelper
  def sketch_form_with(**args, &block)
    form_with(**args, builder: SketchFormBuilder, &block)
  end
end
