module Leaf
  class Base
    delegate :render, to: :@template_object

    def initialize(object_name, method_name, template_object, **options)
      @object_name = object_name
      @method_name = method_name
      @template_object = template_object
      @options = options
    end

    # This is what child classes implement
    def render_leaf
      raise NotImplementedError, "Subclasses must implement a render_leaf method"
    end
  end
end
