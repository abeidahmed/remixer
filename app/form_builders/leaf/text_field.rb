module Leaf
  class TextField < Base
    def render_leaf
      render(Forms::TextFieldComponent.new(@object_name, @method_name, **@options))
    end
  end
end
