module Leaf
  class Label < Base
    def initialize(object_name, method_name, template_object, text = nil, **options)
      super(object_name, method_name, template_object, **options)
      @text = text
    end

    def render_leaf(&block)
      if block
        render(component) { @template_object.capture(&block) }
      else
        render(component)
      end
    end

    private

    def component
      Forms::LabelComponent.new(@object_name, @method_name, @text, **@options)
    end
  end
end
