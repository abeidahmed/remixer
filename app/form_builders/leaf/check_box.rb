module Leaf
  class CheckBox < Base
    def render_leaf
      render(Forms::CheckBoxComponent.new(@object_name, @method_name, **@options))
    end
  end
end
