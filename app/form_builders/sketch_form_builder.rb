class SketchFormBuilder < ActionView::Helpers::FormBuilder
  delegate :render, to: :@template

  def label(method_name, text = nil, **options, &block)
    Leaf::Label.new(object_name, method_name, @template, text, **options).render_leaf(&block)
  end

  def text_field(method_name, **options)
    options[:type] = :text
    Leaf::TextField.new(object_name, method_name, @template, **options).render_leaf
  end

  def email_field(method_name, **options)
    options[:type] = :email
    Leaf::TextField.new(object_name, method_name, @template, **options).render_leaf
  end

  def password_field(method_name, **options)
    options[:type] = :password
    Leaf::TextField.new(object_name, method_name, @template, **options).render_leaf
  end

  def check_box(method_name, **options)
    Leaf::CheckBox.new(object_name, method_name, @template, **options).render_leaf
  end

  def submit(value = nil, **options)
    value ||= submit_default_value
    options[:variant] ||= :primary
    options[:type] ||= :submit
    render(ButtonComponent.new(**options)) { value }
  end
end
