require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Remixer
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Avoid generating these files on scaffold
    config.generators.stylesheets = false
    config.generators.helper = false
    config.generators.helper_specs = false

    config.generators.test_framework :test_unit, fixture: false

    config.to_prepare do
      Devise::SessionsController.layout "slate"
      Devise::RegistrationsController.layout "slate"
      Devise::PasswordsController.layout "slate"
    end

    # Sunday should be the start of the week. Rails defaults to `monday`
    config.beginning_of_week = :sunday

    # ViewComponent
    config.view_component.default_preview_layout = "previews/application"
    config.view_component.show_previews = true

    # Lookbook
    config.lookbook.project_name = "Remixer"
    config.lookbook.debug_menu = true
    config.lookbook.preview_params_options_eval = true
  end
end
