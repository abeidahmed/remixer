Rails.application.routes.draw do
  root "static_pages#home"

  devise_for :users, controllers: {
    registrations: "registrations",
    sessions: "sessions"
  }

  resources :organizations, only: %i[index new create], path: "orgs"

  scope module: :app do
    resources :dashboards, only: :index, path: "dashboard"
  end

  if Rails.env.development?
    mount Lookbook::Engine, at: "/lookbook"
  end
end
