require "test_helper"

class ButtonComponentTest < ViewComponent::TestCase
  test "renders button" do
    render_inline(ButtonComponent.new) { "Button" }

    assert_selector "button[type='button']", text: "Button"
    assert_selector ".btn.btn-default", text: "Button"
  end

  test "primary button" do
    render_inline(ButtonComponent.new(variant: :primary)) { "Button" }

    assert_selector ".btn.btn-primary"
  end

  test "full button" do
    render_inline(ButtonComponent.new(full: true)) { "Button" }

    assert_selector ".btn.w-100"
  end

  test "disabled button" do
    render_inline(ButtonComponent.new(disabled: true)) { "Button" }

    assert_selector "button[disabled]"
    assert_selector "button[aria-disabled='true']"
    assert_selector "button[tabindex='-1']"
    assert_selector ".btn.disabled"
  end

  test "renders anchor" do
    render_inline(ButtonComponent.new(tag: :a, href: "#")) { "Button" }

    assert_selector "a[href='#']", text: "Button"
    refute_selector "[type='button']"
  end

  test "disabled anchor" do
    render_inline(ButtonComponent.new(tag: :a, href: "#", disabled: true)) { "Button" }

    assert_selector "a[disabled]"
    assert_selector "a[aria-disabled='true']"
    assert_selector "a[tabindex='-1']"
    assert_selector ".btn.disabled"

    refute_selector "[href]"
  end

  test "custom class" do
    render_inline(ButtonComponent.new(class: "custom-class")) { "Button" }

    assert_selector ".btn.custom-class"
  end
end
