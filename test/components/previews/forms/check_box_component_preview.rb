class Forms::CheckBoxComponentPreview < ViewComponent::Preview
  # @param disabled toggle
  def default(disabled: false)
    render(Forms::CheckBoxComponent.new(:user, :subscribe, disabled: disabled, checked: true))
  end
end
