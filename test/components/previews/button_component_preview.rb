class ButtonComponentPreview < ViewComponent::Preview
  # @param variant select [default, primary]
  # @param full toggle
  # @param disabled toggle
  def default(variant: ButtonComponent::DEFAULT_VARIANT, full: false, disabled: false)
    render(ButtonComponent.new(variant: variant, full: full, disabled: disabled)) { "Button" }
  end
end
