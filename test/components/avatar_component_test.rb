require "test_helper"

class AvatarComponentTest < ViewComponent::TestCase
  test "renders avatar" do
    render_inline(AvatarComponent.new(src: "https://url.com", alt: "Kitten"))

    assert_selector "img[height='32'][width='32']"
    assert_selector "img[src='https://url.com']"
    assert_selector "img[alt='Kitten']"
    assert_selector "img.avatar.avatar--rounded"
  end

  test "size" do
    render_inline(AvatarComponent.new(src: "https://url.com", alt: "Kitten", size: 28))

    assert_selector "img[height='28'][width='28']"
  end

  test "square avatar" do
    render_inline(AvatarComponent.new(src: "https://url.com", alt: "Kitten", square: true))

    refute_selector ".avatar--rounded"
  end

  test "custom class" do
    render_inline(AvatarComponent.new(src: "https://url.com", alt: "Kitten", class: "custom-class"))

    assert_selector ".avatar.custom-class"
  end
end
