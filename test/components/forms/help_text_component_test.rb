require "test_helper"

class Forms::HelpTextComponentTest < ViewComponent::TestCase
  test "render help text" do
    render_inline(Forms::HelpTextComponent.new) { "Help text" }

    assert_selector "span.form-text", text: "Help text"
  end

  test "custom class" do
    render_inline(Forms::HelpTextComponent.new(class: "custom-help-class")) { "Help text" }

    assert_selector ".form-text.custom-help-class"
  end
end
