require "test_helper"

class Forms::CheckBoxComponentTest < ViewComponent::TestCase
  test "renders checkbox" do
    render_inline(Forms::CheckBoxComponent.new(:user, :subscribe))

    assert_selector "input[type='checkbox'][value='1']"
    assert_selector "input[type='checkbox'][id='user_subscribe']"
    assert_selector "input[type='checkbox'][name='user[subscribe]']"
    assert_selector "input[type='checkbox'][autocomplete='off']"
    assert_selector "input[type='checkbox'].form-check-input"
  end

  test "renders hidden field" do
    render_inline(Forms::CheckBoxComponent.new(:user, :subscribe))

    assert_selector "input[type='hidden']", visible: false
    assert_selector "input[name='user[subscribe]']", visible: false
    assert_selector "input[autocomplete='off']", visible: false
  end
end
