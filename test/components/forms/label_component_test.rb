require "test_helper"

class Forms::LabelComponentTest < ViewComponent::TestCase
  test "renders label" do
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address"))

    assert_selector "label.form-label", text: "Email address"
    assert_selector "label[for='user_email']"
    refute_selector ".form-check-label"
  end

  test "renders block" do
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address")) { "Email" }

    assert_selector "label", text: "Email"
  end

  test "renders checkbox label" do
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address", checkable: true))

    assert_selector "label.form-check-label", text: "Email address"
    refute_selector ".form-label"
  end

  test "renders custom class" do
    render_inline(Forms::LabelComponent.new(:user, :email, "Email address", class: "custom-class"))

    assert_selector ".form-label.custom-class"
  end
end
