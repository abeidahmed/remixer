require "test_helper"

class Forms::TextFieldComponentTest < ViewComponent::TestCase
  test "renders text field" do
    render_inline(Forms::TextFieldComponent.new(:user, :name))

    assert_selector "input[type='text']"
    assert_selector "input[name='user[name]']"
    assert_selector "input[id='user_name']"
    refute_selector "input[aria-describedby]"
  end

  test "help text" do
    render_inline(Forms::TextFieldComponent.new(:user, :name, help_text: "My help text"))

    assert_selector "span.form-text", text: "My help text"

    help_text = page.find("span.form-text")
    assert_selector "input[aria-describedby='#{help_text[:id]}']"
  end

  test "custom class" do
    render_inline(Forms::TextFieldComponent.new(:user, :name, class: "custom-class"))

    assert_selector ".form-control.custom-class"
  end
end
