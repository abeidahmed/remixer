require "test_helper"

Capybara.configure do |config|
  net = Socket.ip_address_list.detect { |addr| addr.ipv4_private? }
  ip = net.nil? ? "localhost" : net.ip_address

  config.always_include_port = true
  config.default_max_wait_time = 5
  config.server_host = ip
end

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include Devise::Test::IntegrationHelpers

  driver_options = {}
  driver_options[:url] = ENV["SELENIUM_REMOTE_URL"] if ENV["SELENIUM_REMOTE_URL"]

  driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400], options: driver_options
end
