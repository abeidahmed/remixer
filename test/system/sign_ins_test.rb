require "application_system_test_case"

class SignInsTest < ApplicationSystemTestCase
  setup do
    @user = create(:user)
  end

  test "redirects to organization index page" do
    visit new_user_session_path
    fill_in "user[email]", with: @user.email
    fill_in "user[password]", with: @user.password
    click_button "Log in"

    assert_current_path organizations_path
  end
end
