require "application_system_test_case"

module Organizations
  class NewOrganizationsTest < ApplicationSystemTestCase
    setup do
      @user = create(:user)
      sign_in(@user)
    end

    test "redirects to dashboard page after creating an organization" do
      visit new_organization_path
      fill_in "organization[name]", with: "My organization"
      click_button "Create"

      assert_current_path dashboards_path
    end
  end
end
