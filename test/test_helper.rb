ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  include FactoryBot::Syntax::Methods
  include Devise::Test::IntegrationHelpers
  include ViewComponent::TestHelpers
end

class ActionDispatch::IntegrationTest
  require "support/json_response"

  include Support::JsonResponse
end

class ViewComponent::TestCase
  require "support/assert"

  include Support::Assert
  include Rails.application.routes.url_helpers
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :minitest
    with.library :rails
  end
end
