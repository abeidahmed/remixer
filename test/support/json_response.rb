module Support
  module JsonResponse
    def json_response
      body = JSON.parse(response.body)

      if body.is_a?(Array)
        body.map { |hash| normalize_hash_key(hash) }
      else
        normalize_hash_key(body)
      end
    end

    def json_response_keys(response = json_response)
      if response.is_a?(Array)
        response.map(&:keys).flatten.uniq
      else
        response.keys
      end
    end

    def json_error(errors = json_response[:errors])
      return unless errors.is_a?(Array)
      errors.map { |error| error[:type].to_sym }.uniq
    end

    private

    def normalize_hash_key(hash)
      hash.deep_transform_keys { |key| key.underscore.to_sym }
    end
  end
end
