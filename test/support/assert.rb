module Support
  module Assert
    def assert_data(selector, attribute:, value:, **options)
      attribute_values = page.find(selector, **options)[attribute].split
      value.split.each { |v| assert_includes(attribute_values, v) }
    end
  end
end
