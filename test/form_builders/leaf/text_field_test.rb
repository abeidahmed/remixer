require "test_helper"

class Leaf::TextFieldTest < ActiveSupport::TestCase
  test "renders text field" do
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.text_field :name
      end
    end

    assert_selector "input[type='text']"
    assert_selector "input[name='user[name]']"
  end
end
