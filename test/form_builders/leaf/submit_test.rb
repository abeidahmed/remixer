require "test_helper"

class Leaf::SubmitTest < ActiveSupport::TestCase
  test "renders submit button" do
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit
      end
    end

    assert_selector "button[type='submit']", text: "Create"
    assert_selector ".btn.btn-primary", text: "Create"
  end

  test "button text" do
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit "Sign up"
      end
    end

    assert_selector "button[type='submit']", text: "Sign up"
  end

  test "custom class" do
    user = build(:user)
    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.submit class: "custom-class"
      end
    end

    assert_selector ".btn.btn-primary.custom-class", text: "Create"
  end
end
