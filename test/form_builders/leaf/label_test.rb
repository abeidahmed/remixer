require "test_helper"

class Leaf::LabelTest < ActiveSupport::TestCase
  test "renders label" do
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.label :email
      end
    end

    assert_selector "label[for='user_email']", text: "Email"
  end

  test "label text" do
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email, "Email address"
      end
    end

    assert_selector "label", text: "Email address"
  end

  test "renders block" do
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email do
          "Your email address"
        end
      end
    end

    assert_selector "label", text: "Your email address"
  end

  test "renders class" do
    render_in_view_context do
      sketch_form_with(url: "/users") do |f|
        f.label :email, class: "foo-label"
      end
    end

    assert_selector "label.form-label.foo-label"
  end
end
