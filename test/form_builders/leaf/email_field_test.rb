require "test_helper"

class Leaf::EmailFieldTest < ActiveSupport::TestCase
  test "renders email field" do
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.email_field :email
      end
    end

    assert_selector "input[type='email']"
    assert_selector "input[name='user[email]']"
  end
end
