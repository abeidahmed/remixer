require "test_helper"

class Leaf::CheckBoxTest < ActiveSupport::TestCase
  test "renders check box" do
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.check_box :subscribe
      end
    end

    assert_selector "input[type='checkbox']"
    assert_selector "input[type='checkbox'][name='user[subscribe]']"
    assert_selector "input[type='checkbox'].form-check-input"
  end

  test "renders hidden field" do
    user = build(:user)

    render_in_view_context do
      sketch_form_with(model: user, url: "/users") do |f|
        f.check_box :subscribe
      end
    end

    assert_selector "input[type='hidden']", visible: false
    assert_selector "input[name='user[subscribe]']", visible: false
    assert_selector "input[autocomplete='off']", visible: false
  end
end
