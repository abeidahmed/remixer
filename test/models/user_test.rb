require "test_helper"

class UserTest < ActiveSupport::TestCase
  subject { build(:user) }

  context "associations" do
    should have_many(:owned_organizations).class_name("Organization").dependent(:destroy)
    should have_many(:memberships).class_name("Member").dependent(:destroy)
  end

  context "validations" do
    should validate_presence_of(:first_name)
    should validate_length_of(:first_name).is_at_most(127)
    should validate_length_of(:last_name).is_at_most(127)
    should validate_presence_of(:time_zone)
    should validate_inclusion_of(:time_zone).in_array(ActiveSupport::TimeZone.all.map(&:name))
  end
end
