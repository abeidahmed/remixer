require "test_helper"

class MemberTest < ActiveSupport::TestCase
  subject { build(:member) }

  context "associations" do
    should belong_to(:organization)
    should belong_to(:user)
  end

  context "validations" do
    should validate_uniqueness_of(:user).scoped_to(:organization_id).with_message("is already in the organization")
  end
end
