require "test_helper"

class OrganizationTest < ActiveSupport::TestCase
  subject { build(:organization) }

  context "associations" do
    should belong_to(:owner).class_name("User")
    should have_many(:organizations).dependent(:destroy)
  end

  context "validations" do
    should validate_presence_of(:name)
    should validate_length_of(:name).is_at_most(255)
  end
end
