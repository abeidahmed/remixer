require "test_helper"

class OrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = create(:user)
    sign_in(@user)
  end

  test "creating an organization" do
    assert_difference "@user.owned_organizations.count", 1 do
      post organizations_path, params: {organization: {name: "My organization"}}
    end

    organization = Organization.last
    assert_equal "My organization", organization.name
    assert_equal @user, organization.owner
  end

  test "creates a member record after creating an organization" do
    assert_difference "@user.memberships.count", 1 do
      post organizations_path, params: {organization: {name: "My organization"}}
    end

    member = Member.last
    assert_equal member.user, @user
  end

  test "creating with invalid params" do
    assert_no_difference "Organization.count" do
      assert_no_difference "Member.count" do
        post organizations_path, params: {organization: {name: ""}}
      end
    end

    assert_equal [:name], json_error
    assert_response :unprocessable_entity
  end
end
