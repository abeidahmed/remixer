FactoryBot.define do
  factory :organization do
    sequence(:name) { |n| "Organization #{n}" }
    association :owner, factory: :user
  end
end
